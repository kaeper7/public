@extends('layouts.app')

@section('content')

<div class="col-lg-7 col-md-9">

@if ($category)
    <h1>Crear Categoría</h1>
    {!! Form::open(['route' =>  ['category.update', $category->id], 'method' => 'PUT']) !!}
@else
    <h1>Crear Categoría</h1>
    {!! Form::open(['route' => 'category.store', 'method' => 'POST']) !!}
@endif

        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            {{ Form::label('name', 'Nombre') }}
            {{ Form::text('name', $category ? $category->name : ''   , ['class' => 'form-control'])}}
            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
            {{ Form::label('description', 'Descrición') }}
            {{ Form::text('description', $category ? $category->description : ''   , ['class' => 'form-control'])}}
            @if ($errors->has('description'))
                <span class="help-block">
                    <strong>{{ $errors->first('description') }}</strong>
                </span>
            @endif
        </div>

    {{ link_to_route('category.index', 'Cancelar', null, ['class'=>'btn btn-danger']) }}
    {{ Form::submit( $category ? 'Editar' : 'Crear',['class'=>'btn btn-primary']) }}

{!! Form::close() !!}

</div>
@endsection
