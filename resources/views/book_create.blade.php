@extends('layouts.app')

@section('content')

<div class="col-lg-7 col-md-9">
<br>


@if ($book)
    <h1>Editar Libro</h1>
    {!! Form::open(['route' =>  ['book.update', $book->id], 'method' => 'PUT']) !!}
@else
    <h1>Crear Libro</h1>
    {!! Form::open(['route' => 'book.store', 'method' => 'POST']) !!}
@endif

        {{ Form::hidden('category_id', $book ? $book->category_id : null) }}
        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            {{ Form::label('name', 'Nombre') }}
            {{ Form::text('name', $book ? $book->name : ''   , ['class' => 'form-control']) }}
            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group{{ $errors->has('author') ? ' has-error' : '' }}">
            {{ Form::label('author', 'Autor') }}
            {{ Form::text('author', $book ? $book->author : ''   , ['class' => 'form-control']) }}
            @if ($errors->has('author'))
                <span class="help-block">
                    <strong>{{ $errors->first('author') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
            {{ Form::label('category_name', 'Categoría') }}
            {{ Form::text('category_name', null, array('placeholder' => 'Ingresa el nombre de una categoría','class' => 'form-control','id'=>'category_name')) }}
            <div id="category-help">
                <strong>Es necesario escoger algo</strong>
            </div>
            @if ($errors->has('category_id'))
                <span class="help-block">
                    <strong>{{ $errors->first('category_id') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group{{ $errors->has('published_at') ? ' has-error' : '' }}">
            {{ Form::label('published_at', 'Publicado') }}
            {{ Form::date('published_at', $book ? $book->published_at : \Carbon\Carbon::now()   , ['class' => 'form-control']) }}
            @if ($errors->has('published_at'))
                <span class="help-block">
                    <strong>{{ $errors->first('published_at') }}</strong>
                </span>
            @endif
        </div>

    {{ link_to_route('book.index', 'Cancelar', null, ['class'=>'btn btn-danger']) }}
    {{ Form::submit( $book ? 'Editar' : 'Crear',['class'=>'btn btn-primary']) }}

{!! Form::close() !!}

</div>
@endsection

@push('scripts')

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
    $(document).ready( function() {
        $("#category_name").autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "{{ URL('catalog/autocomplete') }}",
                    data: { term: request.term },
                    success: function (data) {
                        $('#category_id').val('');
                        $('input[type="submit"]').attr('disabled', true);
                        $('#category-help').show();
                        var re = $.ui.autocomplete.escapeRegex(request.term);
                        var matcher = new RegExp( "\W*(" + re + ")\W*", "i" );
                        var transformed = $.map(data, function (item,index) {
                            var t = item.value.replace(matcher,"<span style='font-weight:bold;color:Blue;'>" + re + "</span>");
                            return {
                                value: t,
                                id: item.id,
                                name: item.value
                            };
                        });
                        response(transformed);
                    },
                    error: function () {
                        response([]);
                    }
                });
            },
            minLength: 2,
            select: function(event, ui) {
                $('input[name="category_id"]').val(ui.item.id);
                $('#category_name').val(ui.item.name);

                $('input[type="submit"]').attr('disabled', false);
                $('#category-help').hide();

                return false;
            }
        }).data("ui-autocomplete")._renderItem = function (ul, item) {
            return $("<li></li>")
                .data("item.autocomplete", item)
                .append("<a>" + item.label + "</a>")
                .appendTo(ul);
        };

    });
</script>
@endpush
