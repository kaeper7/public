<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="myModalLabel">Disponibilidad</h4>
            </div>
            <div class="modal-body">
                <label>¿El libro está disponible?</label>
                <div class="form-group">
                <fieldset id="available">
                    <input type="hidden" name="bookAvailable" id="bookAvailable" value="">
                    <div>
                        {{ Form::radio('available', '1', true) }}
                        <label for="available">Si</label>
                    </div>
                    <div>
                        {{ Form::radio('available', '0') }}
                        <label for="available">No</label>
                    </div>
                </fieldset>
                </div>
            </div>
            <div class="modal-footer">
                <a href="#" data-dismiss="modal" class="btn btn-danger"> Cancelar </a>
                <a href="#" data-dismiss="modal" class="btn btn-primary"> Actualizar </a>
            </div>
        </div>
    </div>
</div>
