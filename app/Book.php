<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    public $fillable = [
        'id',
        'name',
        'author',
        'category_id',
        'published_at',
        'user'
    ];

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

}
