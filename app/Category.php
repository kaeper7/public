<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $fillable = [
        'id',
        'name',
        'description'
    ];

    //Consulta para hacer count de los libros
    public function booksCount()
    {
      return $this->hasOne('App\Book')
        ->selectRaw('category_id, count(*) as aggregate')
        ->groupBy('category_id');
    }


    public function getProductsCountAttribute()
    {
      if ( ! array_key_exists('BooksCount', $this->relations))
          $this->load('BooksCount');

      $related = $this->getRelation('BooksCount');

      return ($related) ? (int) $related->aggregate : 0;
    }
}
