$(function() {

    $(document).ready(function($) {
        $(".clickable-row").click(function() {
            window.location = $(this).parent().data("href");
        });

        $(document).on("click", ".open-AddBookDialog", function () {
            var myBookId = $(this).data('id');
            $(".modal-body #bookAvailable").val( myBookId );
        });

    });

});
